package exception;;

/**
 * Unrecognized Exception
 * @author Chu Van Loc
 */
public class UnrecognizedException extends RuntimeException {
	public UnrecognizedException() {
		super("ERROR: Something went wrowng!");
	}
}

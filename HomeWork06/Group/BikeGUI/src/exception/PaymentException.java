package exception;

/**
 * Payment Exception
 * @author Chu Van Loc
 */
public class PaymentException extends RuntimeException {
    public PaymentException(String s) {
        super(s);
    }
}

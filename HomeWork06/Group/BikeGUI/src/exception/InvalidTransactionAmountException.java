package exception;;

/**
 * Invalid Transaction Amount Exception
 * @author Chu Van Loc
 */
public class InvalidTransactionAmountException extends PaymentException {
	public InvalidTransactionAmountException() {
		super("ERROR: Invalid Transaction Amount!");
	}
}

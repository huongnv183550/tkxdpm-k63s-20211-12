package exception;;

/**
 * Internal server exception
 * @author Chu Van Loc
 */
public class InternalServerErrorException extends PaymentException {

	public InternalServerErrorException() {
		super("ERROR: Internal Server Error!");
	}

}

package exception;;

/**
 * Invalid Version Exception
 * @author Chu Van Loc
 */
public class InvalidVersionException extends PaymentException{
	public InvalidVersionException() {
		super("ERROR: Invalid Version Information!");
	}
}

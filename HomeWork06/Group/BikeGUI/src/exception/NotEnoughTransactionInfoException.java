package exception;

/**
 * Not Enough Transaction Info Exception
 * @author Chu Van Loc
 */
public class NotEnoughTransactionInfoException extends PaymentException {
public NotEnoughTransactionInfoException() {
	super("ERROR: Not Enough Transcation Information");
}
}

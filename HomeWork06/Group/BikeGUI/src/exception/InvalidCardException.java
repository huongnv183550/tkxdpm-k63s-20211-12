package exception;;

/**
 * Invalid Card Exception
 * @author Chu Van Loc
 */
public class InvalidCardException extends PaymentException {
	public InvalidCardException() {
		super("ERROR: Invalid card!");
	}
}

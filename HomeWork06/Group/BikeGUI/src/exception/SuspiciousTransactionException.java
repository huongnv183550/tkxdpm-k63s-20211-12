package exception;;

/**
 * Suspicious Transaction Exception
 * @author Chu Van Loc
 */
public class SuspiciousTransactionException extends PaymentException {
	public SuspiciousTransactionException() {
		super("ERROR: Suspicious Transaction Report!");
	}
}

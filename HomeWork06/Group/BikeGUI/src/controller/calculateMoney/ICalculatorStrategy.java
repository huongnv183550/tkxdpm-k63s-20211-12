package controller.calculateMoney;


/**
 * @author san.dl170111
 */
public interface ICalculatorStrategy {
    int getTotal(long totalTime);
}
